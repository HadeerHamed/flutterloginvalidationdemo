import 'package:flutter/material.dart';
import '../Mixins/ValidationMixin.dart';
class LogInScreen extends StatefulWidget{
    createState(){
      return LogInScreenState();
    }
}
class LogInScreenState extends State<LogInScreen> with ValidationMixin{
  // creating a global key for formWidget
  final formKey = GlobalKey<FormState>();
  // email and password string variables
  String email = '';
  String pass  = '';

  Widget build(context){
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children:[
           emaiField(),
           passwordField(),
           Container(margin: EdgeInsets.only(top: 25.0)),
           submitButton(),
            
          ],
        ),
      ),
    );
  }

  Widget emaiField(){
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email Addresse',
        hintText: 'Enter You Email',
      ),
      validator: emailValidation,
      onSaved:(value){
       email = value;
      },
    );
  }

  Widget passwordField(){
    
     
      return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Password',
        hintText: 'Enter Your Password',
      ),
      validator: passwordValidation ,
      onSaved: (value){
       pass = value;
      },
    );
    
    
  }

  Widget submitButton(){
    return RaisedButton(
      color: Colors.blue,
      child: Text('Submit'),
      onPressed: (){
        // check if every textField is valid it will return true else it will return false
      if (formKey.currentState.validate()){
        // save value in form textFields
       formKey.currentState.save();
       // call api method
      }
      },
      
    );
  }

}